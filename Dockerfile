FROM node

RUN apt update && apt upgrade -y
RUN apt install -y zip unzip
EXPOSE 8000

COPY content/ /content/

WORKDIR /content

RUN npm install

#ENTRYPOINT node /content/server.js
