const archiver = require('archiver')
const fs = require('fs')
const express = require('express')
const app = express()
const path = require('path')
const multer = require('multer')

const port = 8000

const { exec } = require('child_process')

/**
 * @param {String} source
 * @param {String} out
 * @returns {Promise}
 */
function zipDirectory (source, out) {
  const archive = archiver('zip', { zlib: { level: 9 } })
  const stream = fs.createWriteStream(out)

  return new Promise((resolve, reject) => {
    archive
      .directory(source, false)
      .on('error', err => reject(err))
      .pipe(stream)

    stream.on('close', () => resolve())
    archive.finalize()
  })
}

const exists = (dest) => {
  try {
    if (fs.existsSync(path)) {
      return true
    } else {
      return false
    }
  } catch (err) {
    return false
  }
}
app.get('/', (req, res) => {
  const sendDir = (modelsDir) => {
    console.log(`Zipping ${modelsDir}`)
    zipDirectory(modelsDir, '/project/out.zip').then(() => {
      console.log('Sending zip file')
      res.sendFile('/project/out.zip')
    }).catch((e) => {
      res.status(404).send(e.toString())
    })
  }
  let modelsDir = '/project/models'
  if (exists(modelsDir)) {
    fs.readdir(modelsDir, (files) => {
      console.log(files)
      if (files.length === 0) {
        modelsDir += '_backup'
      }
      sendDir(modelsDir)
    })
  } else {
    console.log("Dir doesn't exist")
    sendDir(modelsDir + '_backup')
  }
})
const onFileArrived = `
mv /project/models.zip /project/models_backup.zip
mv /tmp/models.zip /project/models.zip
rm -rf /project/models
unzip /project/models.zip -d /project/models
`
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    console.log('dest required')
    cb(null, '/tmp')
  },
  filename: (req, file, cb) => {
    console.log('hello')
    console.log(file)
    cb(null, 'models.zip')
  }
})
// const upload = multer({ storage })
const upload = multer({ dest: './' })
console.log(upload)
// Upload route
app.post('/', upload.single('models.zip'), (req, res, next) => {
  // console.log(req)
  try {
    res.status(200).send('yey')
    console.log(req.file, req.body)
    /*
        exec(onFileArrived, (err, stdout, stderr) => {
            if (err) {
              //some err occurred
              console.error(err)
              res.status(500).send(err.toString())
            } else {
             // the *entire* stdout and stderr (buffered)
             console.log(`stdout: ${stdout}`);
             console.log(`stderr: ${stderr}`);
              res.status(201).send('File uploded successfully');
            }
        })
        */
  } catch (error) {
    console.error(error)
    res.status(500).send(error.toString())
  }
})

app.listen(port, () => console.log(`Uploader listening on port ${port}!`))
