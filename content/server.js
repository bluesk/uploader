const fs = require('fs')
const app = require('express')()
const { exec } = require('child_process')
const formidable = require('formidable')
const run = (cmd) => new Promise((resolve, reject) => {
  exec(cmd, (err, stdout, stderr) => {
    if (err) {
      reject(err, stderr)
    } else {
      resolve(stdout, stderr)
    }
  })
})

const port = 8000

fs.readdir('/project/', (err, files) => {
  if (err) throw err
  if (files.indexOf('models') === -1) {
    run('mkdir /project/models')
      .then(startServer)
      .catch(err, stderr => {
        console.log(stderr)
        throw err
      })
  } else startServer()
})
const startServer = () => {
  app.get('/', (req, res) => {
    const errCB = (err) => {
      console.log(err)
      res.status(500).send(err.toString())
    }

    const filePath = '/project/models.zip'
    run(`
    cd /project/models
    echo '' > ../models.zip
    rm ../models.zip
    zip -r ../models.zip ./*
    cd -
    `).then(() => {
      const stat = fs.statSync(filePath)
      console.log(`Models zipped, file size: ${stat.size}`)
      res.sendFile(filePath)
      // We replaced all the event handlers with a simple call to readStream.pipe()
      console.log('Models sent.')
    }).catch(errCB)
  })

  app.post('/', (req, res, next) => {
    const errCB = (err) => {
      console.log(err)
      res.status(500).send(err.toString())
    }
    const form = new formidable.IncomingForm()
    form.parse(req, (err, fields, files) => {
      if (err) {
        throw err
      }
      if ('models.zip' in files) {
        const oldpath = files['models.zip'].path
        const newpath = '/project/models.zip'
        run(`mv ${oldpath} ${newpath}`).then(() => {
          console.log('Zip file saved successfully!')
          run(`
          rm -rf /project/models_backup
          mv -f /project/models/ /project/models_backup/
          `)
            .then(() => {
              console.log('Backed up models.')
              run('unzip /project/models.zip -d /project')
                .then(() => {
                  console.log('Successfully unzipped incoming models.')
                  res.end('Success')
                }).catch((err) => {
                  console.log('Error unzipping file!')
                  const unzipError = err.toString()
                  run('cp -r /project/models_backup /project/models').then(() => {
                    console.log('Backup successfully restored.')
                    errCB(`
                    ${unzipError}

                    Backup successfully restored.`)
                  }).then((err) => {
                    console.log('Error restoring backup!')
                    errCB(`
                    ${unzipError}

                    ${err}`)
                  })
                  errCB(err)
                })
            }).catch(errCB)
        }).catch(errCB)
      } else {
        errCB('Bad file name')
      }
    })
  })

  app.listen(port, () => console.log(`Uploader listening on port ${port}!`))
}
