docker stop uploader;
docker rm uploader;
docker run \
    -p 8000:8000 \
    --name=uploader \
    -v $PWD/content:/content \
    -v /cloud_data/giulio/files/Documents/transformers_text_generation/former_v3:/project \
    -d \
    uploader node server.js
: '
docker run \
    -p 8000:8000 \
    --name=uploader \
    -v $PWD/content:/content \
    -v /cloud_data/giulio/files/Documents/transformers_text_generation/former_v3:/project \
    -d \
    uploader bash
'
